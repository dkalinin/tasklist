angular.module('util', [])
    .factory('Util', function() {
        return {
            generateId: function () {
                return Math.floor((Math.random() * 10000) + 1);
            },
            formatDate: function (date) {
                return date.getFullYear() + '-' + ('0' + (date.getMonth() + 1)).slice(-2) + '-'
                    + ('0' + date.getDate()).slice(-2) + ": " + date.getHours() + ":" + date.getMinutes()
                    + ":" + date.getSeconds();
            },
            taskComparator: function (o1, o2) {
                if (o1.isDone && !o2.isDone) {
                    return 1;
                }
                if (o2.isDone && !o1.isDone) {
                    return -1;
                }
                return o2.date - o1.date;
            }
        };
    })