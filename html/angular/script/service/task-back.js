angular.module('task-back', ['util'])
    .factory('TaskService', function (Util) {
        var taskService;

        function Model(data) {
            var taskStorage = [];
            var storage = new TaskStorage();
            var taskList = new TaskList(data, storage);

            function Task(date, description, author, assignedTo, isDone) {
                this.date = date;
                this.description = description;
                this.author = author;
                this.assignedTo = assignedTo;
                this.isDone = isDone;
                this.id = Util.generateId();
            }

            function TaskList(data, externalStorage) {
                var _storage = [];
                var _externalStorage = externalStorage;

                this.add = function (task) {
                    _storage.push(task);
                    var tasks = this.getAll();
                    _externalStorage.save(tasks);
                    return tasks;
                };

                this.remove = function (taskId) {
                    for (var i = 0; i < _storage.length; i++) {
                        if (_storage[i].id === taskId) {
                            _storage.splice(i, 1);
                            break;
                        }
                    }
                    var tasks = this.getAll();
                    _externalStorage.save(tasks);
                    return tasks;
                };

                this.setIsDone = function (taskId, value) {
                    for (var i = 0; i < _storage.length; i++) {
                        if (_storage[i].id === taskId) {
                            _storage[i].isDone = value;
                            break;
                        }
                    }
                    var tasks = this.getAll();
                    _externalStorage.save(tasks);
                    return tasks;
                };

                this.editDescription = function (task) {
                    for (var i = 0; i < _storage.length; i++) {
                        if (_storage[i].id === task.id) {
                            _storage[i].description = task.description;
                            break;
                        }
                    }
                    _externalStorage.save(taskList.getAll());
                };

                this.getAll = function () {
                    return _storage.slice();
                };

                this.setNewData = function (data) {
                    _storage = data;
                    return this.getAll();
                };

                if (externalStorage) {
                    this.setNewData(_externalStorage.getData());
                }

                if (data) {
                    this.setNewData(data);
                }
            }

            function TaskStorage() {
                var _internalStorage = localStorage;
                var _storageKey = 'taskList';
                this.save = function (data) {
                    var array = [];
                    for (var i = 0; i < data.length; i++) {
                        array.push(data[i]);
                    }
                    _internalStorage.setItem(_storageKey, JSON.stringify(array));
                };
                this.getData = function () {
                    var data = _internalStorage.getItem(_storageKey);
                    var storage = [];
                    if (data) {
                        data = JSON.parse(data);
                        if (data) {
                            for (var i = 0; i < data.length; i++) {
                                var elem = new Task(new Date(Date.parse(data[i].date)), data[i].description, data[i].author,
                                    data[i].assignedTo, data[i].isDone);
                                elem.id = data[i].id;
                                storage.push(elem);
                            }
                        }
                    }
                    return storage;
                };

                this.setKey = function (value) {
                    _storageKey = value;
                }
            }

            this.setStorageKey = function (value) {
                storage.setKey(value);
                taskList.setNewData(storage.getData());
            };

            this.add = function (data) {
                var newTask = new Task(data.date, data.description, data.author, data.assignedTo, false);
                taskList.add(newTask);
                angular.copy(this.getAll(), taskStorage);
                console.log("Task added:" + data.description + " " + data.assignedTo);
            };

            this.remove = function (data) {
                taskList.remove(data.id);
                angular.copy(this.getAll(), taskStorage);
            };

            this.complete = function (data) {
                taskList.setIsDone(data.id, true);
                angular.copy(this.getAll(), taskStorage);
            };

            this.reopen = function (data) {
                taskList.setIsDone(data.id, false);
                angular.copy(this.getAll(), taskStorage);
            };

            this.editDescription = function (data) {
                taskList.editDescription(data);
                angular.copy(this.getAll(), taskStorage);
            };

            this.getAll = function () {
                return taskList.getAll();
            };

            taskStorage = this.getAll();
            this.tasks = taskStorage;
        }

        taskService = new Model();
        return taskService;
    });