angular.module('task-front', ['task-back', 'util'])
    .controller('createTaskCtrl', function ($scope, $rootScope, TaskService) {
        $scope.addTask = function () {
            var CURRENT_USER = "me";
            var assignedToValue = $scope.assignedTo;
            if (!$scope.assignedTo || $scope.assignedTo == CURRENT_USER) {
                assignedToValue = $scope.user;
            }
            var data = {
                date: new Date(),
                description: $scope.description,
                assignedTo: assignedToValue,
                author: $scope.user
            };
            TaskService.add(data);
            console.log("Add task:" + $scope.description + " " + assignedToValue);
            $scope.description = '';
            $scope.assignedTo = '';
        };
    })
    .controller('showTasksCtrl', function ($scope,  $rootScope, TaskService, Util) {
        $scope.user = $rootScope.user;
        $scope.format = function (date) {
            return Util.formatDate(date);
        };
        $scope.deleteTask = function (id) {
            TaskService.remove({id: id});
            console.log("Task deleted:" + id);
        };
        $scope.complete = function (id) {
            TaskService.complete({id: id});
            console.log("Task done:" + id);
        };
        $scope.reopen = function (id) {
            TaskService.reopen({id: id});
            console.log("Task reopened:" + id);
        };
        $scope.editTask = function (task) {
            TaskService.editDescription(task);
            $scope.editMode = false;
            console.log("Task edited:" + task.id + ": " + task.description);
        };
    })
    .controller('showTasksToMeCtrl', function ($scope, TaskService, Util) {
        $scope.tasks = function () {
            var result = TaskService.tasks;
            return jQuery.grep(result,function (element) {
                return element.assignedTo == $scope.user;
            }).sort(Util.taskComparator);
        };
    })
    .controller('showTasksByMeCtrl', function ($scope, TaskService, Util) {
        $scope.tasks = function () {
            var result = TaskService.tasks;
            return jQuery.grep(result,function (element) {
                return element.author == $scope.user && element.assignedTo != $scope.user;
            }).sort(Util.taskComparator);
        };
    });
