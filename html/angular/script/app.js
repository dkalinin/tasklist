angular.module("app", ['task-front','task-back','ui.router'])
    .config(function ($stateProvider, $urlRouterProvider) {
        $urlRouterProvider.otherwise("/tasks/to-me");
        $stateProvider
            .state('tasks', {
                views: {
                    'create': {
                        templateUrl: '../pages/tasks/create.html',
                        controller:'createTaskCtrl'
                    },
                    'list': {
                        templateUrl: '../pages/tasks/tasks.html',
                        controller:'showTasksCtrl'
                    }
                },
                url: "/tasks"
            })
            .state('tasks.to-me', {
                url: "/to-me",
                templateUrl:'../pages/tasks/list.html',
                controller:'showTasksToMeCtrl'
            })
            .state('tasks.by-me', {
                url: "/by-me",
                templateUrl:'../pages/tasks/list.html',
                controller:'showTasksByMeCtrl'
            })
    })
    .controller('initCtrl', function($scope, TaskService){
        $scope.setUser = function(user) {
            TaskService.setUser(user);
        }
    });