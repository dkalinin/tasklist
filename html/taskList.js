function TaskListWidget() {

    var widget;
    var widgetUser;

    var model;
    var controller;
    var view;

    var $eventBus;

    this.setEventBus = function(newObject){
        $eventBus = newObject;
    };

    var CURRENT_USER_ID = "me";


    var Util = {
        generateId: function () {
            return Math.floor((Math.random() * 10000) + 1);
        },
        formatDate: function (date) {
            return date.getFullYear() + '-' + ('0' + (date.getMonth() + 1)).slice(-2) + '-'
                + ('0' + date.getDate()).slice(-2) + ": " + date.getHours() + ":" + date.getMinutes()
                + ":" + date.getSeconds();
        },
        taskComparator: function (o1, o2) {
            if (o1.isDone() && !o2.isDone()) {
                return 1;
            }
            if (o2.isDone() && !o1.isDone()) {
                return -1;
            }
            return o2.date() - o1.date();
        }
    };

    var widgetId = Util.generateId();

    this.Events = {
        ADD_TASK: 'addTask',
        DELETE_TASK: 'deleteTask',
        ADD_TASK_TO_MODEL: 'addTaskToModel',
        RENDER_VIEW: 'renderView',
        DELETE_TASK_FROM_MODEL: 'deleteTaskFromModel',
        TASK_ADDED: 'taskAdded',
        TASK_DELETED: 'taskDeleted',
        DONE_TASK: 'doneTask',
        SET_TASK_AS_DONE: 'setTaskAsDone',
        TASK_CHANGED: 'taskChanged',
        UNDONE_TASK: 'unDoneTask',
        SET_TASK_AS_NOT_DONE: 'setTaskAsNotDone',
        GET_SAVED_DATA: 'getSavedData',
        IMPORT_DATA_TO_MODEL: 'importDataToModel',
        EDIT_TASK_VIEW: 'editTaskView',
        EDIT_TASK_MODEL: 'editTaskModel'
    };

    var events = this.Events;

    this.init = function (options) {
        widget = options.wid;
        widgetUser = options.user;

        if(options.eventBus){
            $eventBus = options.eventBus;
        } else {
            $eventBus = $({});
        }

        controller = new Controller();
        view = new View();

        model = new Model(options.initData);
        if(options.storageKey){
            model.setStorageKey(options.storageKey);
        }

    };

    /* *** MODEL *** */
    function Model(data) {

        var storage = new TaskStorage();
        var taskList = new TaskList(data, storage);


        function Task(date, description, author, assignedTo, isDone) {
            var _id = Util.generateId();
            var _date = date;
            var _description = description;
            var _author = author;
            var _assignedTo = assignedTo;
            var _isDone = isDone;

            this.id = function (value) {
                if (typeof value != "undefined")
                    _id = value;
                return _id;
            };
            this.date = function (value) {
                if (typeof value != "undefined")
                    _date = value;
                return _date;
            };
            this.description = function (value) {
                if (typeof value != "undefined")
                    _description = value;
                return _description;
            };
            this.author = function (value) {
                if (typeof value != "undefined")
                    _author = value;
                return _author;
            };
            this.assignedTo = function (value) {
                if (typeof value != "undefined")
                    _assignedTo = value;
                return _assignedTo;
            };
            this.isDone = function (value) {
                if (typeof value != "undefined")
                    _isDone = value;
                return _isDone;
            };
        }

        function TaskList(data, externalStorage) {
            var _storage = [];
            var _externalStorage = externalStorage;

            this.add = function (task) {
                if (task.assignedTo() == CURRENT_USER_ID || task.assignedTo() == "") {
                    task.assignedTo(widgetUser);
                }
                if (task.author() == CURRENT_USER_ID || task.author() == "") {
                    task.author(widgetUser);
                }
                _storage.push(task);
                var tasks = this.getAll();
                _externalStorage.save(tasks);
                $eventBus.trigger(events.TASK_ADDED, {taskList: tasks});
            };

            this.remove = function (taskId) {
                for (var i = 0; i < _storage.length; i++) {
                    if (_storage[i].id() === taskId) {
                        _storage.splice(i, 1);
                        break;
                    }
                }
                var tasks = this.getAll();
                _externalStorage.save(tasks);
                $eventBus.trigger(events.TASK_DELETED, {taskList: tasks});
            };

            this.setIsDone = function (taskId, value) {
                for (var i = 0; i < _storage.length; i++) {
                    if (_storage[i].id() === taskId) {
                        _storage[i].isDone(value);
                        break;
                    }
                }
                var tasks = this.getAll();
                _externalStorage.save(tasks);
                $eventBus.trigger(events.TASK_CHANGED, {taskList: tasks});
            };

            this.editDescription = function (taskId, value) {
                for (var i = 0; i < _storage.length; i++) {
                    if (_storage[i].id() === taskId) {
                        _storage[i].description(value);
                        break;
                    }
                }
                _externalStorage.save(taskList.getAll());
            };

            this.getAll = function () {
                return _storage.slice();
            };

            this.setNewData = function (data) {
                _storage = data;
                $eventBus.trigger(events.TASK_CHANGED, {taskList: this.getAll()});
            };

            if(externalStorage) {
                this.setNewData(_externalStorage.getData());
            }

            if (data) {
                this.setNewData(data);
            }

            $eventBus.trigger(events.TASK_CHANGED, {taskList: this.getAll()});
        }

        function TaskStorage() {
            var _internalStorage = localStorage;
            var _storageKey = 'taskList';
            this.save = function (data) {
                var array = [];
                for (var i = 0; i < data.length; i++) {
                    array.push({
                        id: data[i].id(), date: data[i].date(), description: data[i].description(),
                        assignedTo: data[i].assignedTo(), author: data[i].author(), isDone: data[i].isDone()
                    });
                }
                _internalStorage.setItem(_storageKey, JSON.stringify(array));
            };
            this.getData = function () {
                var data = _internalStorage.getItem(_storageKey);
                if (data) {
                    data = JSON.parse(data);
                    var storage = [];
                    if(data){
                        for (var i = 0; i < data.length; i++) {
                            var elem = new Task(new Date(Date.parse(data[i].date)), data[i].description, data[i].author,
                                data[i].assignedTo, data[i].isDone);
                            elem.id(data[i].id);
                            storage.push(elem);
                        }
                    }
                }
                return storage;
            };

            this.setKey = function (value) {
                _storageKey = value;
            }
        }

        this.setStorageKey = function (value) {
            storage.setKey(value);
            taskList.setNewData(storage.getData());
        };

        $eventBus.on(events.ADD_TASK_TO_MODEL, function (event, data) {
            var newTask = new Task(data.date, data.description, data.author, data.assignedTo, false);
            taskList.add(newTask);
        });

        $eventBus.on(events.DELETE_TASK_FROM_MODEL, function (event, data) {
            taskList.remove(data.id);
        });

        $eventBus.on(events.SET_TASK_AS_DONE, function (event, data) {
            taskList.setIsDone(data.id, true);
        });

        $eventBus.on(events.SET_TASK_AS_NOT_DONE, function (event, data) {
            taskList.setIsDone(data.id, false);
        });
/*
        $eventBus.on(events.IMPORT_DATA_TO_MODEL, function (event, data) {
            taskList = new TaskList(storage.getData());
        });
 */
        $eventBus.on(events.EDIT_TASK_MODEL, function (event, data) {
            taskList.editDescription(data.id, data.newDescription);
        });

    }


    /* *** CONTROLLER *** */

    function Controller() {
        $eventBus.on(events.ADD_TASK, function (event, data) {
            $eventBus.trigger(events.ADD_TASK_TO_MODEL, data);
        });
        $eventBus.on(events.TASK_ADDED, function (event, data) {
            $eventBus.trigger(events.RENDER_VIEW, data);
        });
        $eventBus.on(events.TASK_DELETED, function (event, data) {
            $eventBus.trigger(events.RENDER_VIEW, data);
        });
        $eventBus.on(events.DELETE_TASK, function (event, data) {
            $eventBus.trigger(events.DELETE_TASK_FROM_MODEL, data);
        });
        $eventBus.on(events.DONE_TASK, function (event, data) {
            $eventBus.trigger(events.SET_TASK_AS_DONE, data);
        });
        $eventBus.on(events.TASK_CHANGED, function (event, data) {
            $eventBus.trigger(events.RENDER_VIEW, data);
        });
        $eventBus.on(events.UNDONE_TASK, function (event, data) {
            $eventBus.trigger(events.SET_TASK_AS_NOT_DONE, data);
        });
        $eventBus.on(events.GET_SAVED_DATA, function (event, data) {
            $eventBus.trigger(events.IMPORT_DATA_TO_MODEL, data);
        });
        $eventBus.on(events.EDIT_TASK_VIEW, function (event, data) {
            $eventBus.trigger(events.EDIT_TASK_MODEL, data);
        });

    }

    /* *** VIEW *** */
    function View() {
        var divPanel = $('<div class="panel panel-success container"></div>');
        var panelHeading = $('<div class="panel-heading row"><h3 class="panel-title">Create Task</h3></div>');
        divPanel.append(panelHeading);
        widget.append(divPanel);

        var descriptionInput = $('<textarea id="description" class="form-control" name="description" rows="3" ' +
            'placeholder="Enter description"/>');
        var assignedToInput = $('<input id="assignedTo" class="form-control" name="assignedTo" type="text"/>');
        var addButton = $('<input type="button" class="btn btn-success" value="Add Task" disabled>');

        var form = $('<form class="form-horizontal"></form>');
        var formGroupDesc = $('<div class="form-group"></div>');
        var formGroupAssign = formGroupDesc.clone();
        var descriptionLabel = $('<label for="description" class="col-sm-2 control-label">Description</label>');
        var assignedToLabel = $('<label for="assignedTo" class="col-sm-2 control-label">Assigned to</label>');

        formGroupAssign.append(assignedToLabel, $('<div class="col-sm-10">').append(assignedToInput));
        formGroupDesc.append(descriptionLabel, $('<div class="col-sm-10">').append(descriptionInput));
        form.append(formGroupDesc, formGroupAssign, $('<div class="form-group">')
                               .append($('<div class="col-sm-offset-2 col-sm-10"></div>').append(addButton)));
        var panelBody = $('<div class="panel-body"></div>');
        panelBody.append($('<div class="row"></div>').append(form));
        divPanel.append(panelBody);
        panelBody.append($('<hr/>'));

        var taskListView = $('<ul class="taskList list-group"></ul>');
        var noTasksText = $('<p class="text-center">You have no tasks yet</p>');

        panelBody.append($('<div class="row"></div>').append(taskListView));

        descriptionInput.on("keyup", function () {
            if ($(this).val().trim()) {
                addButton.prop("disabled", false);
            } else {
                addButton.prop("disabled", true);
            }
        });

        addButton.on("click", function (e) {
            addTaskHandler();
        });

        function addTaskHandler() {
            var date = new Date();
            $eventBus.trigger(events.ADD_TASK, {
                date: date,
                description: descriptionInput.val(),
                author: widgetUser,
                assignedTo: assignedToInput.val()
            });
        }

        $eventBus.on(events.RENDER_VIEW, function (event, data) {
            if (data) {
                var list = data.taskList;
                taskListView.empty();
                if (list) {
                    list.sort(Util.taskComparator);
                    for (var i = 0; i < list.length; i++) {
                        if (list[i].assignedTo() === widgetUser) {
                            var taskView = new TaskView(list[i].id(), list[i].date(), list[i].description(), list[i].author(),
                                list[i].isDone());
                            taskListView.append(taskView.dom());
                        }
                    }
                }
            }
            if (taskListView.children().length == 0) {
                taskListView.append(noTasksText);
            }
            descriptionInput.val("");
            assignedToInput.val(CURRENT_USER_ID);
            addButton.prop("disabled", true);
        });

        function TaskView(id, date, description, author, isDone) {
            var _id = id;
            var _date = date;
            var _description = description;
            var _author = author;
            var _isDone = isDone;

            this.id = function (value) {
                if (typeof value != "undefined")
                    _id = value;
                return _id;
            };
            this.description = function (value) {
                if (typeof value != "undefined")
                    _description = value;
                return _description;
            };
            this.dom = function () {
                var view = $('<li class="task list-group-item row"></li>');
                var taskDescDiv = $('<div class=""></div>');
                var taskDescription = $("<div class=''>" + _description + "</div>");
                taskDescDiv.append(taskDescription);
                taskDescDiv.append("<small>" + Util.formatDate(_date) + "</small>");
                view.append(taskDescDiv);
                if (author != widgetUser) {
                    view.append("<br/>");
                    view.append("<p class='list-group-item-text'>Author: " + _author + "</p>");
                }
                var buttonDiv = $('<div class="btn-group pull-right " style="top: 100%;transform: translateY(-100%);"></div>');
                view.append($('<div class="overlay"></div>').append(buttonDiv));
                var buttonDelete = $('<input type="button" class="btn btn-danger" value="Delete" style=" display: none"/>');
                buttonDelete.on("click", function (e) {
                    $eventBus.trigger(events.DELETE_TASK, {id: _id});
                });
                buttonDiv.append(buttonDelete);
                if (_isDone) {
                    var buttonUnDone = $('<input type="button" class="btn btn-default" value="UnDone" style="display: none"/>');
                    buttonUnDone.on("click", function (e) {
                        $eventBus.trigger(events.UNDONE_TASK, {id: _id});
                    });
                    buttonDiv.append(buttonUnDone);
                } else {
                    var buttonDone = $('<input type="button" class="btn btn-success" value="Done" style="display: none"/>');
                    buttonDone.on("click", function (e) {
                        $eventBus.trigger(events.DONE_TASK, {id: _id});
                    });
                    buttonDiv.append(buttonDone);
                }
                view.hover(function () {
                    $(this).find(":button").show();
                }, function () {
                    $(this).find(":button").hide();
                });
                if (isDone) {
                    view.addClass("disabled");
                    taskDescDiv.addClass("done");
                }

                taskDescription.on("click", function(){     //inline edit
                    var div = $(this);
                    var editDiv = $('<div></div>');
                    var textarea = $('<textarea style="width: 100%; height: 100%"></textarea>');
                    var oldValue = div.html();
                    textarea.val(div.html());
                    var ok = $('<button class="btn btn-default" disabled="true">OK</button> ');
                    var cancel = $('<button class="btn btn-default">Cancel</button> ');
                    editDiv.append(textarea).append($("<div class='btn-group pull-left'></div>").append(ok).append(cancel));
                    div.parent().append(editDiv);
                    div.hide();
                    ok.on("click", function() {
                        div.html(textarea.val());
                        $eventBus.trigger(events.EDIT_TASK_VIEW, {id: _id, newDescription: textarea.val()});
                        div.show();
                        editDiv.remove();
                    });
                    cancel.on("click", function() {
                        div.html(oldValue);
                        div.show();
                        editDiv.remove();
                    });
                    textarea.on("keyup", function () {
                        if ($(this).val().trim()) {
                            ok.prop("disabled", false);
                        } else {
                            ok.prop("disabled", true)
                        }
                    });
                });

                return view;
            }
        }
    }

}

