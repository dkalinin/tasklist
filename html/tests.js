function unitTest() {
    unitjs
        .case('model: add task', function () {
            var $eventBus = $({});
            var storageKey = 'taskListTest1';
            localStorage.setItem(storageKey, null);

            var widget = new TaskListWidget();

            var user = 'TestUser';

            widget.init({
                wid: $('<div></div>'),
                user: user,
                eventBus: $eventBus,
                storageKey: storageKey
            });

            var date = new Date;
            var description = "Description";
            var author = "me";
            var assignedTo = "me";

            var eventFired = false;
            var eventData = null;
            $eventBus.on(widget.Events.TASK_ADDED, function (event, data) {
                eventFired = true;
                eventData = data;
            });

            setTimeout(function () {
                unitjs.assert(eventFired, 'Event ' + widget.Events.TASK_ADDED + ' did not fire in 100 ms.');
            }, 100);    //timeout with an error in 100 ms

            $eventBus.trigger(widget.Events.ADD_TASK_TO_MODEL,
                {date: date, description: description, author: author, assignedTo: assignedTo});

            setTimeout(function () {
                var result = JSON.parse(localStorage.getItem(storageKey));
                var resultArray = unitjs.array(result);
                resultArray.isNotEmpty();
                resultArray.hasLength(1);

                var objInResult = unitjs.object(result[0]);

                objInResult.isNotEmpty();
                objInResult.hasProperties(["date", "description", "author", "assignedTo", "id", "isDone"]);
                objInResult.hasProperty("description", description);
                objInResult.hasProperty("author", user);
                objInResult.hasProperty("assignedTo", user);
                objInResult.hasProperty("isDone", false);

                unitjs.object(new Date(Date.parse(result[0].date))).is(date);
                unitjs.value(result[0].id).isNumber();
                unitjs.number(result[0].id).isNotInfinite();

                unitjs.object(eventData).hasProperty("taskList");
                unitjs.array(eventData.taskList).isNotEmpty();
                unitjs.array(eventData.taskList).hasLength(1);

                var returnedTask = eventData.taskList[0];

                unitjs.object(returnedTask).isNotEmpty();
                unitjs.object(returnedTask).hasProperties(["date", "description", "author", "assignedTo", "id", "isDone"]);
                unitjs.assert(returnedTask.id() == result[0].id);
                unitjs.assert(returnedTask.date() == date);
                unitjs.assert(returnedTask.description() == description);
                unitjs.assert(returnedTask.assignedTo() == user);
                unitjs.assert(returnedTask.author() == user);
                unitjs.assert(returnedTask.isDone() == false);
            }, 100);

            setTimeout(function () {
                localStorage.removeItem(storageKey);
            }, 300);
        })

        .case('model: delete task', function () {
            var $eventBus = $({});
            var storageKey = 'taskListTest2';
            var user = 'TestUser';
            var id = 1053;

            localStorage.setItem(storageKey, '[{"id":' + id + ',"date":"2015-04-22T14:51:02.251Z",' +
            '"description":"Description","assignedTo":"TestUser","author":"TestUser","isDone":false}]');

            var widget = new TaskListWidget();
            widget.init({
                wid: $('<div></div>'),
                user: user,
                eventBus: $eventBus,
                storageKey: storageKey
            });

            var eventFired = false;
            var eventData = null;
            $eventBus.on(widget.Events.TASK_DELETED, function (event, data) {
                eventFired = true;
                eventData = data;
            });

            setTimeout(function () {
                unitjs.assert(eventFired, 'Event ' + widget.Events.TASK_ADDED + ' did not fire in 100 ms.');
            }, 100);    //timeout with an error in 100 ms

            $eventBus.trigger(widget.Events.DELETE_TASK_FROM_MODEL, {id: id});

            setTimeout(function () {
                var result = JSON.parse(localStorage.getItem(storageKey));
                var resultArray = unitjs.array(result);
                resultArray.isEmpty();

                unitjs.array(eventData.taskList).isEmpty();
            }, 100);

            setTimeout(function () {
                localStorage.removeItem(storageKey);
            }, 300);
        })

        .case('model: set task as done', function () {
            var $eventBus = $({});
            var storageKey = 'taskListTest3';
            var user = 'TestUser';
            var id = 1053;

            localStorage.setItem(storageKey, '[{"id":' + id + ',"date":"2015-04-22T14:51:02.251Z",' +
            '"description":"Description","assignedTo":"TestUser","author":"TestUser","isDone":false}]');

            var widget = new TaskListWidget();
            widget.init({
                wid: $('<div></div>'),
                user: user,
                eventBus: $eventBus,
                storageKey: storageKey
            });

            var eventFired = false;
            var eventData = null;
            $eventBus.on(widget.Events.TASK_CHANGED, function (event, data) {
                eventFired = true;
                eventData = data;
            });

            setTimeout(function () {
                unitjs.assert(eventFired, 'Event ' + widget.Events.TASK_CHANGED + ' did not fire in 100 ms.');
            }, 100);    //timeout with an error in 100 ms

            $eventBus.trigger(widget.Events.SET_TASK_AS_DONE, {id: id});

            setTimeout(function () {
                var result = JSON.parse(localStorage.getItem(storageKey));
                var resultArray = unitjs.array(result);
                resultArray.isNotEmpty();
                resultArray.hasLength(1);
                unitjs.value(result[0].isDone).is(true);
            }, 100);

            setTimeout(function () {
                localStorage.removeItem(storageKey);
            }, 300);
        });
}
